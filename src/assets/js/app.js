/**
 * File app.js.
 *
 * Library initialization and custom scripts
 * Variable "as" is name of "Alvarium Site"
 */

let as = {
  init: function() {
    as.aos();
    as.owl_carousel();
    as.masonry();
  },

  aos: function() {
    AOS.init();
  },

  owl_carousel: function() {

    let carousel = {
      type1: $('.as-carousel.type-1')
    };

    /* Type 1 */
    if(carousel.type1.length) {
      let data = {};

      data = {
        loop: true,
        autoplay: false,
        dots: false,
        nav: true,
        margin: 25
      }

      data.responsive = {
        0: { items: 1},
        600: { items: 2},
        1000: { items: 2},
      }

      carousel.type1.owlCarousel(data);
    }

    /* Type 2 */
    //....
  },

}

$(document).ready(function() {
  as.init();
});